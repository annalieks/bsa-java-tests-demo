package com.example.demo;

import com.example.demo.dto.ToDoCategory;
import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
public class DemoApplicationLayersIT {

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ToDoRepository toDoRepository;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value("Wash the dishes"))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1l))
                .andExpect(jsonPath("$[0].category").doesNotExist())
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Test
    void whenGetByText_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(get("/todos/search")
                .param("text", "wash"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value("Wash the dishes"))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1l))
                .andExpect(jsonPath("$[0].category").doesNotExist())
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Test
    void whenPostWithIdNotFound_thenReturnNotFoundResponse() throws Exception {
        var toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.id = 1000l;
        toDoSaveRequest.text = "Does not exist";

        this.mockMvc
                .perform(post("/todos")
                .content(objectMapper.writeValueAsString(toDoSaveRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenPostWithoutId_thenReturnAddedEntity() throws Exception {
        var toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.text = "New to do";

        ResultActions result = this.mockMvc
                .perform(post("/todos")
                        .content(objectMapper.writeValueAsString(toDoSaveRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(toDoSaveRequest.text))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.category").doesNotExist())
                .andExpect(jsonPath("$.completedAt").doesNotExist());

        var response = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                ToDoResponse.class
        );
        var addedEntity = toDoRepository.findById(response.id);

        assertTrue(addedEntity.isPresent());
        assertEquals(addedEntity.get().getText(), response.text);

        toDoRepository.deleteById(response.id);
    }

    @Test
    void whenPostWithExistingId_thenReturnUpdatedEntity() throws Exception {
        var testEntity = new ToDoEntity(3l, "To do text", "TestCategory");
        testEntity = toDoRepository.save(testEntity);

        var toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.text = "Updated to do";
        toDoSaveRequest.id = testEntity.getId();

        ResultActions result = this.mockMvc
                .perform(post("/todos")
                        .content(objectMapper.writeValueAsString(toDoSaveRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(toDoSaveRequest.text))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(toDoSaveRequest.id))
                .andExpect(jsonPath("$.category").value(toDoSaveRequest.category))
                .andExpect(jsonPath("$.completedAt").doesNotExist());

        var response = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                ToDoResponse.class
        );

        var updatedEntity = toDoRepository.findById(response.id);

        assertTrue(updatedEntity.isPresent());
        assertEquals(updatedEntity.get().getText(), response.text, toDoSaveRequest.text);
        assertEquals(updatedEntity.get().getCategory(), response.category, toDoSaveRequest.category);

        toDoRepository.deleteById(response.id);
    }

    @Test
    void whenGetByCategory_thenReturnValidResponse() throws Exception {
        var testEntity = new ToDoEntity(3l, "To do text", "TestCategory");
        testEntity = toDoRepository.save(testEntity);

        this.mockMvc
                .perform(get(String.format("/todos/category/%s", testEntity.getCategory())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testEntity.getText()))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(testEntity.getId()))
                .andExpect(jsonPath("$[0].category").value(testEntity.getCategory()))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());

        toDoRepository.deleteById(testEntity.getId());
    }

    @Test
    void whenPatchWithIdNotFound_thenReturnNotFoundResponse() throws Exception {
        var toDoCategory = new ToDoCategory();
        toDoCategory.category = "TestCategory";
        Long id = 100l;

        this.mockMvc
                .perform(patch(String.format("/todos/category/%d", id))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(toDoCategory)))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenPatchWithExistingId_thenReturnUpdatedEntityResponse() throws Exception {
        var testEntity = new ToDoEntity(4l, "To do text");
        testEntity = toDoRepository.save(testEntity);

        ToDoCategory toDoCategory = new ToDoCategory();
        toDoCategory.category = "TestCategory";

        this.mockMvc
                .perform(patch(String.format("/todos/category/%d", testEntity.getId()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDoCategory)))
                .andExpect(status().isNoContent());

        var updatedEntity = toDoRepository.findById(testEntity.getId());

        assertTrue(updatedEntity.isPresent());
        assertEquals(toDoCategory.category, updatedEntity.get().getCategory(), toDoCategory.category);

        toDoRepository.deleteById(testEntity.getId());
    }

    @Test
    void whenPutWithExistingId_thenReturnUpdatedEntityResponse() throws Exception {
        var testEntity = new ToDoEntity(4l, "To do text");
        testEntity = toDoRepository.save(testEntity);
        var startTime = ZonedDateTime.now();

        this.mockMvc
                .perform(put(String.format("/todos/%d/complete", testEntity.getId())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(testEntity.getText()))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(testEntity.getId()))
                .andExpect(jsonPath("$.category").isEmpty())
                .andExpect(jsonPath("$.completedAt").exists());

        var updatedEntity = toDoRepository.findById(testEntity.getId());

        assertTrue(updatedEntity.isPresent());
        assertThat(updatedEntity.get().getCompletedAt().isAfter(startTime));

        toDoRepository.deleteById(testEntity.getId());
    }

    @Test
    void whenDeleteById_thenReturnValidResponse() throws Exception {
        var testEntity = new ToDoEntity(5l, "To do text");
        testEntity = toDoRepository.save(testEntity);

        this.mockMvc
                .perform(delete(String.format("/todos/%d", testEntity.getId())))
                .andExpect(status().isOk());

        var deletedEntity = toDoRepository.findById(testEntity.getId());

        assertTrue(deletedEntity.isEmpty());
    }

}
