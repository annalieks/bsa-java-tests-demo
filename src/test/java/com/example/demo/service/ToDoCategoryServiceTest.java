package com.example.demo.service;

import com.example.demo.dto.ToDoCategory;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.util.ArrayList;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ToDoCategoryServiceTest {

    private ToDoRepository toDoRepository;

    private ToDoCategoryService toDoCategoryService;

    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        toDoCategoryService = new ToDoCategoryService(toDoRepository);
    }

    @Test
    void whenGetByCategory_thenReturnAllInCategory() {
        String category = "category";
        var testToDos = new ArrayList<ToDoEntity>();
        testToDos.add(new ToDoEntity(0l, "Test 1", category));
        testToDos.add(new ToDoEntity(1l, "Test 2", category));

        when(toDoRepository.findByCategoryLike(anyString())).thenReturn(testToDos);

        var todos = toDoCategoryService.getByCategory(category);

        assertEquals(todos.size(), testToDos.size());
        for (int i = 0; i < todos.size(); i++) {
            assertThat(todos.get(i), samePropertyValuesAs(
                    ToDoEntityToResponseMapper.map(testToDos.get(i))
            ));
        }
    }

    @Test
    void whenSetCategoryWithId_thenToDoCategoryIsUpdated() throws ToDoNotFoundException {
        var toDo = new ToDoEntity(0l, "test", "testCategory");
        var testCategory = new ToDoCategory();
        testCategory.category = "testCategoryUpdated";

        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(toDo));

        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        toDoCategoryService.setCategory(toDo.getId(), testCategory);

        assertEquals(toDo.getCategory(), testCategory.category);
    }

    @Test
    void whenSetCategoryWithIdNotFound_thenThrowNotFoundException() {
        var testCategoryDto = new ToDoCategory();
        testCategoryDto.category = "test";

        assertThrows(ToDoNotFoundException.class,
                () -> toDoCategoryService.setCategory(1l, testCategoryDto));
    }

}
