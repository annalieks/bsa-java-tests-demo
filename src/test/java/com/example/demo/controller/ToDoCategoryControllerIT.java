package com.example.demo.controller;

import com.example.demo.dto.ToDoCategory;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoCategoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoCategoryController.class)
@ActiveProfiles(profiles = "test")
public class ToDoCategoryControllerIT {

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoCategoryService toDoCategoryService;

    @Test
    void whenGetByCategory_thenReturnValidResponse() throws Exception {
        String testText = "My to do text", category = "test";
        when(toDoCategoryService.getByCategory(anyString())).thenReturn(
                Arrays.asList(
                        ToDoEntityToResponseMapper.map(new ToDoEntity(0l, testText, category))
                )
        );

        this.mockMvc
                .perform(get(String.format("/todos/category/%s", category)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].category").value(category))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Test
    void whenPatchWithIdNotFound_thenReturnNotFoundMessage() throws Exception {
        var category = new ToDoCategory();
        category.category = "category";
        Long id = 0l;

        doThrow(new ToDoNotFoundException(id))
                .when(toDoCategoryService).setCategory(anyLong(), ArgumentMatchers.any(ToDoCategory.class));

        this.mockMvc
                .perform(patch(String.format("/todos/category/%d", id))
                .content(objectMapper.writeValueAsString(category))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenPatchWithExistingId_thenReturnValidResponse() throws Exception {
        var category = new ToDoCategory();
        category.category = "category";
        var toDo = new ToDoEntity(0l, "Test text");

        this.mockMvc
                .perform(patch(String.format("/todos/category/%d", toDo.getId()))
                .content(objectMapper.writeValueAsString(category))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

}
