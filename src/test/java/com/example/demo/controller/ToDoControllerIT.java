package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.Arrays;

import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ToDoService toDoService;

	@Test
	void whenGetAll_thenReturnValidResponse() throws Exception {
		String testText = "My to do text";
		Long testId = 1l;
		when(toDoService.getAll()).thenReturn(
			Arrays.asList(
				ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
			)
		);

		this.mockMvc
			.perform(get("/todos"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$").isArray())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].text").value(testText))
			.andExpect(jsonPath("$[0].id").isNumber())
			.andExpect(jsonPath("$[0].id").value(testId))
			.andExpect(jsonPath("$[0].category").doesNotExist())
			.andExpect(jsonPath("$[0].completedAt").doesNotExist());
	}

	@Test
	void whenGetCompleted_thenReturnValidResponse() throws Exception {
		String testText = "My to do text";
		Long testId = 1l;
		when(toDoService.getCompleted()).thenReturn(
				Arrays.asList(
						ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
				)
		);

		this.mockMvc
				.perform(get("/todos/completed"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].text").value(testText))
				.andExpect(jsonPath("$[0].id").isNumber())
				.andExpect(jsonPath("$[0].id").value(testId))
				.andExpect(jsonPath("$[0].category").doesNotExist())
				.andExpect(jsonPath("$[0].completedAt").doesNotExist());

	}

	@Test
	void whenGetByText_thenReturnValidResponse() throws Exception {
		String testText = "My to do text", searchText = "text";
		Long testId = 1l;
		when(toDoService.getByText(anyString())).thenReturn(
				Arrays.asList(
						ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
				)
		);

		this.mockMvc
				.perform(get("/todos/search")
				.param("text", searchText))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].text").value(testText))
				.andExpect(jsonPath("$[0].id").isNumber())
				.andExpect(jsonPath("$[0].id").value(testId))
				.andExpect(jsonPath("$[0].category").doesNotExist())
				.andExpect(jsonPath("$[0].completedAt").doesNotExist());

	}

	@Test
	void whenPostToDoWithoutId_thenReturnValidResponse() throws Exception {
		var toDoSaveRequest = new ToDoSaveRequest();
		toDoSaveRequest.text = "My to do text";
		Long newId = 0l;
		when(toDoService.upsert(ArgumentMatchers.any(ToDoSaveRequest.class))).thenReturn(
				ToDoEntityToResponseMapper.map(new ToDoEntity(newId, toDoSaveRequest.text))
		);

		this.mockMvc
				.perform(post("/todos")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(toDoSaveRequest)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.text").value(toDoSaveRequest.text))
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(newId))
				.andExpect(jsonPath("$.category").doesNotExist())
				.andExpect(jsonPath("$.completedAt").doesNotExist());
	}

	@Test
	void whenPostToDoWithExistingId_thenReturnValidResponse() throws Exception {
		var toDoSaveRequest = new ToDoSaveRequest();
		toDoSaveRequest.text = "My to do text";
		toDoSaveRequest.id = 0l;
		when(toDoService.upsert(ArgumentMatchers.any(ToDoSaveRequest.class))).thenReturn(
				ToDoEntityToResponseMapper.map(new ToDoEntity(toDoSaveRequest.id, toDoSaveRequest.text))
		);

		this.mockMvc
				.perform(post("/todos")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(toDoSaveRequest)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.text").value(toDoSaveRequest.text))
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(toDoSaveRequest.id))
				.andExpect(jsonPath("$.category").doesNotExist())
				.andExpect(jsonPath("$.completedAt").doesNotExist());
	}

	@Test
	void whenPutWithIdNotFound_thenReturnNotFoundResponse() throws Exception {
		Long id = 0l;

		when(toDoService.completeToDo(anyLong())).thenThrow(new ToDoNotFoundException(anyLong()));

		this.mockMvc
				.perform(put(String.format("/todos/%d/complete", id)))
				.andExpect(status().isNotFound());
	}

	@Test
	void whenPutWithExistingId_thenReturnValidResponse() throws Exception {
		var expectedToDo = new ToDoEntity(0l, "New Item");

		when(toDoService.completeToDo(anyLong())).thenAnswer(i -> {
			expectedToDo.completeNow();
			return ToDoEntityToResponseMapper.map(expectedToDo);
		});

		this.mockMvc
				.perform(put(String.format("/todos/%d/complete", expectedToDo.getId())))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.text").value(expectedToDo.getText()))
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(expectedToDo.getId()))
				.andExpect(jsonPath("$.category").doesNotExist())
				.andExpect(jsonPath("$.completedAt").exists());
	}

}
