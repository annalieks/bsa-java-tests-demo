package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.Arrays;
import java.util.Optional;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerWithServiceIT {

	ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ToDoRepository toDoRepository;

	@Test
	void whenGetAll_thenReturnValidResponse() throws Exception {
		String testText = "My to do text";
		when(toDoRepository.findAll()).thenReturn(
			Arrays.asList(
				new ToDoEntity(1l, testText)
			)
		);
		
		this.mockMvc
			.perform(get("/todos"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$").isArray())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].text").value(testText))
			.andExpect(jsonPath("$[0].id").isNumber())
			.andExpect(jsonPath("$[0].category").doesNotExist())
			.andExpect(jsonPath("$[0].completedAt").doesNotExist());
	}

	@Test
	void whenGetByText_thenReturnValidResponse() throws Exception {
		String testText = "test", testToDoText = "My to do " + testText;

		when(toDoRepository.findByTextContainsIgnoreCase(testText)).thenReturn(
				Arrays.asList(
						new ToDoEntity(1l, testToDoText)
				)
		);

		this.mockMvc
				.perform(get("/todos/search")
						.param("text", testText))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].text").value(testToDoText))
				.andExpect(jsonPath("$[0].id").isNumber())
				.andExpect(jsonPath("$[0].category").doesNotExist())
				.andExpect(jsonPath("$[0].completedAt").doesNotExist());
	}

	@Test
	void whenPostWithExistingId_thenReturnValidResponse() throws Exception {
		var toDoSaveRequest = new ToDoSaveRequest();
		toDoSaveRequest.id = 1l;
		toDoSaveRequest.text = "My to do text";

		when(toDoRepository.findById(anyLong()))
				.thenReturn(Optional.of(new ToDoEntity(toDoSaveRequest.id, toDoSaveRequest.text)));

		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class)))
				.thenAnswer(i -> i.getArguments()[0]);

		this.mockMvc
				.perform(post("/todos")
						.content(objectMapper.writeValueAsString(toDoSaveRequest))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.text").value(toDoSaveRequest.text))
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(toDoSaveRequest.id))
				.andExpect(jsonPath("$.category").doesNotExist())
				.andExpect(jsonPath("$.completedAt").doesNotExist());
	}

	@Test
	void whenPutWithExistingId_thenReturnValidResponse() throws Exception {
		Long id = 100l;
		String testText = "test";

		when(toDoRepository.findById(anyLong()))
				.thenReturn(Optional.of(new ToDoEntity(id, testText)));

		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class)))
				.thenAnswer(i -> i.getArguments()[0]);

		this.mockMvc
				.perform(put(String.format("/todos/%d/complete", id)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.text").value(testText))
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(id))
				.andExpect(jsonPath("$.category").doesNotExist())
				.andExpect(jsonPath("$.completedAt").exists());
	}

	@Test
	void whenDeleteByExistingId_thenReturnValidResponse() throws Exception {
		var id = 0l;

		this.mockMvc
				.perform(delete(String.format("/todos/%d", id)))
				.andExpect(status().isOk())
		;
		verify(toDoRepository, times(1)).deleteById(anyLong());
	}

	@Test
	void whenDeleteCompleted_thenReturnValidResponse() throws Exception {
		this.mockMvc
				.perform(delete("/todos/completed"))
				.andExpect(status().isOk())
		;
		verify(toDoRepository, times(1)).deleteByCompletedAtNotNull();
	}

}
