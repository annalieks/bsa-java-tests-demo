package com.example.demo.controller;

import com.example.demo.dto.ToDoCategory;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoCategoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoCategoryController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoCategoryService.class)
public class ToDoCategoryControllerWithServiceIT {

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Test
    void whenGetByCategory_thenReturnValidResponse() throws Exception {
        String testText = "My to do text", testCategory = "category";
        when(toDoRepository.findByCategoryLike(anyString())).thenReturn(
                Arrays.asList(
                        new ToDoEntity(1l, testText, testCategory)
                )
        );

        this.mockMvc
                .perform(get(String.format("/todos/category/%s", testCategory)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].category").value(testCategory))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Test
    void whenPatchWithIdNotFound_thenReturnNotFoundResponse() throws Exception {
        Long id = 0l;
        var toDoCategory = new ToDoCategory();
        toDoCategory.category = "category";

        this.mockMvc
                .perform(patch(String.format("/todos/category/%d", id))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(toDoCategory)))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenPatchWithExistingId_thenReturnValidResponse() throws Exception {
        Long id = 0l;
        var toDoCategory = new ToDoCategory();
        toDoCategory.category = "category";

        when(toDoRepository.findById(anyLong())).thenReturn(
                Optional.of(new ToDoEntity(id, "test", toDoCategory.category))
        );

        this.mockMvc
                .perform(patch(String.format("/todos/category/%d", id))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(toDoCategory)))
                .andExpect(status().isNoContent());
    }

}
