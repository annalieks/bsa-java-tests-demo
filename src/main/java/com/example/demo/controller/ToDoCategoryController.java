package com.example.demo.controller;

import com.example.demo.dto.ToDoCategory;
import com.example.demo.dto.ToDoResponse;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.service.ToDoCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/todos/category")
public class ToDoCategoryController {

    @Autowired
    ToDoCategoryService toDoCategoryService;

    @ExceptionHandler({ ToDoNotFoundException.class })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleException(Exception ex) {
        return ex.getMessage();
    }

    @GetMapping("/{category}")
    @Valid List<ToDoResponse> getByCategory(@PathVariable String category) {
        return toDoCategoryService.getByCategory(category);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    void setCategory(@PathVariable Long id, @RequestBody ToDoCategory toDoCategory) throws ToDoNotFoundException {
        toDoCategoryService.setCategory(id, toDoCategory);
    }

}
