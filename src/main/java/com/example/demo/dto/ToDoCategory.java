package com.example.demo.dto;

import javax.validation.constraints.NotNull;

public class ToDoCategory {
    @NotNull
    public String category;
}
