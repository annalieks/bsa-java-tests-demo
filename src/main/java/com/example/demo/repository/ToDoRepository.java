package com.example.demo.repository;

import com.example.demo.model.ToDoEntity;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ToDoRepository extends JpaRepository<ToDoEntity, Long> {
    List<ToDoEntity> findByCompletedAtNotNull();

    List<ToDoEntity> findByTextContainsIgnoreCase(String text);

    List<ToDoEntity> findByCategoryLike(String category);

    void deleteByCompletedAtNotNull();
}