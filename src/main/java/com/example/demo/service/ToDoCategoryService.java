package com.example.demo.service;

import com.example.demo.dto.ToDoCategory;
import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.repository.ToDoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ToDoCategoryService {

    private final ToDoRepository toDoRepository;

    public ToDoCategoryService(ToDoRepository toDoRepository) {
        this.toDoRepository = toDoRepository;
    }

    public List<ToDoResponse> getByCategory(String category) {
        return toDoRepository.findByCategoryLike(category).stream()
                .map(ToDoEntityToResponseMapper::map).collect(Collectors.toList());
    }

    public void setCategory(Long id, ToDoCategory toDoCategory) throws ToDoNotFoundException {
        var todo = toDoRepository.findById(id).orElseThrow(() -> new ToDoNotFoundException(id));
        todo.setCategory(toDoCategory.category);
        toDoRepository.save(todo);
    }
}
